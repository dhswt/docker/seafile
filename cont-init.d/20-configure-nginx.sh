#!/usr/bin/with-contenv bash
set -e

# envsubst \$NGINX_LISTEN_PORT, < /config/nginx.conf > /etc/nginx/conf.d/default.conf
cp /config/nginx.conf /etc/nginx/conf.d/default.conf
