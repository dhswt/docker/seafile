#!/usr/bin/with-contenv bash
set -e

CONF_DIR="$DATA_DIR/conf"

if [[ ! -d "$CONF_DIR" ]]; then
    echo "WARNING: Skipping config update, config-dir '$CONF_DIR' does not exist";
    exit 0
fi

# update seahub_settings.py
if [ -f "$CONF_DIR/seahub_settings.py" ]; then

    # user
    MATCH_STRING="'USER':[[:space:]]*'.*'"
    REPLACE_STRING="'USER': '$MYSQL_USER'"
    sed -i 's/'"$MATCH_STRING"'/'"$REPLACE_STRING"'/g' "$CONF_DIR/seahub_settings.py" 

    # password
    MATCH_STRING="'PASSWORD':[[:space:]]*'.*'"
    REPLACE_STRING="'PASSWORD': '$MYSQL_USER_PASSWD'"
    sed -i 's/'"$MATCH_STRING"'/'"$REPLACE_STRING"'/g' "$CONF_DIR/seahub_settings.py" 

    # host
    MATCH_STRING="'HOST':[[:space:]]*'.*'"
    REPLACE_STRING="'HOST': '$MYSQL_HOST'"
    sed -i 's/'"$MATCH_STRING"'/'"$REPLACE_STRING"'/g' "$CONF_DIR/seahub_settings.py" 

    # port
    MATCH_STRING="'PORT':[[:space:]]*'.*'"
    REPLACE_STRING="'PORT': '$MYSQL_PORT'"
    sed -i 's/'"$MATCH_STRING"'/'"$REPLACE_STRING"'/g' "$CONF_DIR/seahub_settings.py"

    # dbname
    MATCH_STRING="'NAME':[[:space:]]*'.*'"
    REPLACE_STRING="'NAME': '$SEAHUB_DB'"
    sed -i 's/'"$MATCH_STRING"'/'"$REPLACE_STRING"'/g' "$CONF_DIR/seahub_settings.py"  

fi

# update ccnet.conf
if [ -f "$CONF_DIR/ccnet.conf" ]; then
    sed -i "s|^SERVICE_URL = .*$|SERVICE_URL = https://$SERVER_IP|"     $CONF_DIR/ccnet.conf
    sed -i "s|^HOST = .*$|HOST = $MYSQL_HOST|"                          $CONF_DIR/ccnet.conf
    sed -i "s|^PORT = .*$|PORT = $MYSQL_PORT|"                          $CONF_DIR/ccnet.conf
    sed -i "s|^USER = .*$|USER = $MYSQL_USER|"                          $CONF_DIR/ccnet.conf
    sed -i "s|^PASSWD = .*$|PASSWD = $MYSQL_USER_PASSWD|"               $CONF_DIR/ccnet.conf
    sed -i "s|^DB = .*$|DB = $CCNET_DB|"                                $CONF_DIR/ccnet.conf
fi

# update seafevents.conf
if [ -f "$CONF_DIR/seafevents.conf" ]; then
    sed -i "s|^host = .*$|host = $MYSQL_HOST|"                          $CONF_DIR/seafevents.conf
    sed -i "s|^port = .*$|port = $MYSQL_PORT|"                          $CONF_DIR/seafevents.conf
    sed -i "s|^username = .*$|username = $MYSQL_USER|"                  $CONF_DIR/seafevents.conf
    sed -i "s|^password = .*$|password = $MYSQL_USER_PASSWD|"           $CONF_DIR/seafevents.conf
    sed -i "s|^name = .*$|name = $CCNET_DB|"                            $CONF_DIR/seafevents.conf
fi

# update seafile.conf
if [ -f "$CONF_DIR/seafile.conf" ]; then
    sed -i "s|^host = .*$|host = $MYSQL_HOST|"                          $CONF_DIR/seafevents.conf
    sed -i "s|^port = .*$|port = $MYSQL_PORT|"                          $CONF_DIR/seafevents.conf
    sed -i "s|^user = .*$|user = $MYSQL_USER|"                          $CONF_DIR/seafevents.conf
    sed -i "s|^password = .*$|password = $MYSQL_USER_PASSWD|"           $CONF_DIR/seafevents.conf
    sed -i "s|^db_name = .*$|db_name = $CCNET_DB|"                      $CONF_DIR/seafevents.conf
fi
