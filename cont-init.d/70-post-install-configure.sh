#!/usr/bin/with-contenv bash
set -e

cd $SEAFILE_BIN

# only run once post-install
if [ $POST_INSTALL_SCRIPTS != "1" ]; then
	exit 0
fi

# safe installed version
echo "${SEAFILE_VERSION}" > "${INSTALLED_VERSION_FILE}"

# configure URLs
echo "FILE_SERVER_ROOT = 'https://${SERVER_IP}/seafhttp'" >> $DATA_DIR/conf/seahub_settings.py
sed -i "s|^SERVICE_URL = .*$|SERVICE_URL = https://${SERVER_IP}|" $DATA_DIR/conf/ccnet.conf

# append settings to existing files
# - enable syslog for seafevents, seafile, seahub
echo "Running post-install: append configuration"

cat /config/seahub_settings.append.py >> $DATA_DIR/conf/seahub_settings.py
cat /config/seafevents.append.conf >> $DATA_DIR/conf/seafevents.conf
cat /config/seafile.append.conf >> $DATA_DIR/conf/seafile.conf

# disable elasticsearch
cat $DATA_DIR/conf/seafevents.conf \
	| tr '\n' '\r' \
	| sed -e 's/\[INDEX\ FILES\]\renabled = true/\[INDEX\ FILES\]\renabled = false\rexternal_es_server = true/' \
	| tr '\r' '\n' \
	> $DATA_DIR/conf/seafevents2.conf
mv $DATA_DIR/conf/seafevents2.conf $DATA_DIR/conf/seafevents.conf

 # disable mail sending
cat $DATA_DIR/conf/seafevents.conf \
    | tr '\n' '\r' \
    | sed -e 's/\[SEAHUB\ EMAIL\]\renabled = true/\[SEAHUB\ EMAIL\]\renabled = false\r/' \
    | tr '\r' '\n' \
    > $DATA_DIR/conf/seafevents2.conf
mv $DATA_DIR/conf/seafevents2.conf $DATA_DIR/conf/seafevents.conf

# increase default files per library limit
cat $DATA_DIR/conf/seafile.conf \
	| tr '\n' '\r' \
	| sed -e 's/\[fileserver]\r/\[fileserver]\rmax_sync_file_count = 1000000\r/' \
	| tr '\r' '\n' \
	> $DATA_DIR/conf/seafile2.conf
mv $DATA_DIR/conf/seafile2.conf $DATA_DIR/conf/seafile.conf
