#!/usr/bin/with-contenv bash
set -e


# container debug
CONTAINER_DEBUG="${CONTAINER_DEBUG:-0}"
set-contenv CONTAINER_DEBUG "$CONTAINER_DEBUG"

# seafile binary dir
SEAFILE_BIN="${INSTALL_DIR}/seafile-pro-server-${SEAFILE_VERSION}"
set-contenv SEAFILE_BIN "$SEAFILE_BIN"

# data dir
DATA_DIR="/data"
set-contenv DATA_DIR "$DATA_DIR"

# set SEAFILE_DIR, it used by seafile to store data
SEAFILE_DIR=$INSTALL_DIR/seafile-data
set-contenv SEAFILE_DIR "$SEAFILE_DIR"

# install version file
INSTALLED_VERSION_FILE="${DATA_DIR}/version"
set-contenv INSTALLED_VERSION_FILE "$INSTALLED_VERSION_FILE"

# installed version
if [ -f $INSTALLED_VERSION_FILE ]
then
	INSTALLED_VERSION=$(cat $INSTALLED_VERSION_FILE)
	set-contenv INSTALLED_VERSION "$INSTALLED_VERSION"
else
	set-contenv INSTALLED_VERSION "NONE"
fi

# control flag for post-installation scripts
POST_INSTALL_SCRIPTS=0
set-contenv POST_INSTALL_SCRIPTS "$POST_INSTALL_SCRIPTS"

# cron enable
ENABLE_CRON_GC="${ENABLE_CRON_GC:-0}"
set-contenv ENABLE_CRON_GC "$ENABLE_CRON_GC"

# user id
#SEAFILE_UID="${SEAFILE_UID:-1000}"
#set-contenv SEAFILE_UID "$SEAFILE_UID"

# group id
#SEAFILE_GID="${SEAFILE_GID:-1000}"
#set-contenv SEAFILE_GID "$SEAFILE_GID"


# various
set-contenv SETUP_MODE "${SETUP_MODE:-auto}"
set-contenv NGINX_LISTEN_PORT "${NGINX_LISTEN_PORT:-80}"

# database
DATABASE_TYPE="${DATABASE_TYPE:-sqlite}"
set-contenv DATABASE_TYPE "$DATABASE_TYPE"

if [ $DATABASE_TYPE == "mysql" ]; then
	set-contenv MYSQL_USER_HOST "${MYSQL_USER_HOST:-%}"
	set-contenv MYSQL_USER "${MYSQL_USER:-seafile}"
	set-contenv MYSQL_PORT "${MYSQL_PORT:-3306}"

	# support db prefix
	DB_PREFIX="${DB_PREFIX:-seafile_}"
	set-contenv DB_PREFIX "$DB_PREFIX"
	set-contenv CCNET_DB "${DB_PREFIX}${CCNET_DB:-ccnet}"
	set-contenv SEAFILE_DB "${DB_PREFIX}${SEAFILE_DB:-seafile}"
	set-contenv SEAHUB_DB "${DB_PREFIX}${SEAHUB_DB:-seahub}"

	# require MYSQL_ROOT_PASSWD
	[ -z "$MYSQL_ROOT_PASSWD" ] && echo "Variable MYSQL_ROOT_PASSWD is required" && exit 1;
	set-contenv MYSQL_ROOT_PASSWD "${MYSQL_ROOT_PASSWD}"

	# require MYSQL_HOST
	[ -z "$MYSQL_HOST" ] && echo "Variable MYSQL_HOST is required" && exit 1;
	set-contenv MYSQL_HOST "${MYSQL_HOST}"

	# require MYSQL_USER_PASSWD
	[ -z "$MYSQL_USER_PASSWD" ] && echo "Variable MYSQL_USER_PASSWD is required" && exit 1;
	set-contenv MYSQL_USER_PASSWD "${MYSQL_USER_PASSWD}"

	# require MYSQL_ROOT_PASSWD
	[ -z "$MYSQL_ROOT_PASSWD" ] && echo "Variable MYSQL_ROOT_PASSWD is required" && exit 1;
	set-contenv MYSQL_ROOT_PASSWD "${MYSQL_ROOT_PASSWD}"
fi



# require SERVER_NAME as the default would be the container id which is not very useful
[ -z "$SERVER_NAME" ] && echo "Variable SERVER_NAME is required" && exit 1;
set-contenv SERVER_NAME "${SERVER_NAME}"

# require SERVER_IP as the default ip would most likely not work
[ -z "$SERVER_IP" ] && echo "Variable SERVER_IP is required" && exit 1;
set-contenv SERVER_IP "${SERVER_IP}"


# require SEAFILE_ADMIN_MAIL
[ -z "$SEAFILE_ADMIN_MAIL" ] && echo "Variable SEAFILE_ADMIN_MAIL is required" && exit 1;
set-contenv SEAFILE_ADMIN_MAIL "${SEAFILE_ADMIN_MAIL}"

# require SEAFILE_ADMIN_PASS
[ -z "$SEAFILE_ADMIN_PASS" ] && echo "Variable SEAFILE_ADMIN_PASS is required" && exit 1;
set-contenv SEAFILE_ADMIN_PASS "${SEAFILE_ADMIN_PASS}"