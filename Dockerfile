FROM    registry.gitlab.com/dhswt/docker/s6-base:debian-buster-stable
LABEL   maintainer="David Hiendl <david.hiendl@dhswt.de>"

# new version download: https://download.seafile.com/d/6e5297246c/?p=/pro
ENV     INSTALL_DIR=/opt/seafile \
        SEAFILE_UID=1000 \
        SEAFILE_GID=1000 \
        SEAFILE_VERSION="8.0.4" \
        ENABLE_CRON=1 \
        S6_BEHAVIOUR_IF_STAGE2_FAILS=2 \
        LC_ALL=en_US.UTF-8 \
        LANG=en_US.UTF-8 \
        LANGUAGE=en_US:en

EXPOSE  80
VOLUME  /data
WORKDIR /opt/seafile

# install basic required utils
RUN     DEBIAN_FRONTEND=noninteractive \
        BUILD_DEPS=" \
            lsb-release \
            curl \
            wget \
            gnupg2 \
            build-essential \
            python3-pip \
            python3-dev \
        " \
&&      apt-get update \
&&      apt-get -y install --no-install-recommends $BUILD_DEPS \
            ca-certificates \
#
# add nginx repo
&&      echo "deb http://nginx.org/packages/mainline/debian `lsb_release -cs` nginx" | tee /etc/apt/sources.list.d/nginx.list \
&&      curl -fsSL https://nginx.org/keys/nginx_signing.key | apt-key add - \
#
# workaround for: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=863199
&&      mkdir -p /usr/share/man/man1 \
#
# workaround for no-java setup
&&      touch /usr/bin/java \
&&      chmod +x /usr/bin/java \
#
# install dependencies 
# - skipping: openjdk-11-jre-headless
&&      apt-get update \
&&      apt-get install -y --no-install-recommends \
            default-libmysqlclient-dev \
            cron \
            gettext-base \
            locales \
            poppler-utils \
            python3 \
            sqlite3 \
            tzdata \
            nginx \
#
# workaround for locale errors
&&      sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen \
&&      locale-gen \
#
# install python dependencies
&&      pip3 install --upgrade \
            wheel \
            setuptools \
&&      pip3 install --timeout=3600 \
            Pillow \
            pylibmc \
            captcha \
            jinja2 \
            sqlalchemy \
            django-pylibmc \
            django-simple-captcha \
            python3-ldap \
            future \
            mysqlclient \
\
# download seafile
&&      mkdir -p $INSTALL_DIR \
&&      cd $INSTALL_DIR \
&&      wget -q -O seafile.tar.gz "https://download.seafile.com/d/6e5297246c/files/?p=/pro/seafile-pro-server_${SEAFILE_VERSION}_x86-64_Ubuntu.tar.gz&dl=1" \
&&      tar xfz seafile.tar.gz \
&&      rm seafile.tar.gz \
# reduce image size be removing built-in elasticsearch
&&      rm -rf ${INSTALL_DIR}/seafile-pro-server-${SEAFILE_VERSION}/pro/elasticsearch \
\
# add user with consistent uid and gid
&&      groupadd seafile \
            -r \
            -g $SEAFILE_GID \
&&      useradd seafile \
            -r \
            -s /bin/bash \
            -M \
            -u $SEAFILE_UID \
            -g $SEAFILE_GID \
            -d $INSTALL_DIR \
&&      chown -R seafile:seafile $INSTALL_DIR \
\
# clean up pip as it is no longer required
&&      apt-get remove -y --purge $BUILD_DEPS \
&&      apt-get autoremove -y --purge \
\
# clean up cache
&&      apt-get clean \
&&      rm -rf \
            /var/lib/apt/lists/* \
            /tmp/* \
            /var/tmp/* \            
            /home/.cache \
            /root/.cache \
            /usr/share/doc/* \
&&       find / -type f -name '*.py[co]' -delete -or -type d -name '__pycache__' -delete

# install services, scripts, config
ADD     services.d  /etc/services.d
ADD     cont-init.d /etc/cont-init.d
ADD     config      /config
ADD     cron        /etc/cron.d
ADD     bin         /usr/local/sbin

# update permissions
RUN     chmod 555 \
            /etc/cont-init.d/* \
            /usr/local/sbin/* \
            /etc/services.d/seafile/* \
            /etc/services.d/seahub/* \
            /etc/services.d/nginx/* \
&&      chmod 0644 \
            /etc/cron.d/*
