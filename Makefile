default: image

docker-image = registry.gitlab.com/dhswt/docker/seafile
docker-tag = pro-dev

image:
	docker build \
		--squash \
		-t $(docker-image):$(docker-tag) \
		.
push: image
	docker push $(docker-image):$(docker-tag)

enter:
	-docker rm -f seafile
	docker run \
		-ti \
		-e CONTAINER_DEBUG=1 \
		-e SERVER_NAME=TESTSERVER \
		-e SERVER_IP=127.0.0.1 \
		-e SEAFILE_ADMIN_MAIL=test@dhswt.de \
		-e SEAFILE_ADMIN_PASS=Temp12345 \
		-p 8000:8000 \
		--name seafile \
		--entrypoint bash \
		$(docker-image):$(docker-tag)


run:
	docker-compose down -v
	docker-compose pull db
	docker-compose up -d db
	sleep 10
	docker-compose up seafile

show-images:
	docker images | grep "$(docker-image)"

# Remove dangling images
clean-images:
	docker images -a -q \
		--filter "reference=$(docker-image)" \
		--filter "dangling=true" \
	| xargs docker rmi

# Remove all images
clear-images:
	docker images -a -q \
		--filter "reference=$(docker-image)" \
	| xargs docker rmi
